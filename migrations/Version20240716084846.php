<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240716084846 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
      /*   $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638D7A6F6E0 FOREIGN KEY (type_de_site_id) REFERENCES type_de_site (id)');
        $this->addSql('CREATE INDEX IDX_4C62E638D7A6F6E0 ON contact (type_de_site_id)');
        $this->addSql('ALTER TABLE equipe_dev ADD en_biographie LONGTEXT DEFAULT NULL, ADD en_image_alt VARCHAR(255) DEFAULT NULL, CHANGE biographie fr_biographie LONGTEXT NOT NULL, CHANGE image_alt fr_image_alt VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE forfait ADD en_nom VARCHAR(255) DEFAULT NULL, ADD en_sous_titre VARCHAR(255) DEFAULT NULL, ADD en_avantages JSON DEFAULT NULL, ADD en_description LONGTEXT DEFAULT NULL, CHANGE nom fr_nom VARCHAR(255) NOT NULL, CHANGE avantages fr_avantages JSON NOT NULL, CHANGE description fr_description LONGTEXT NOT NULL, CHANGE sous_titre fr_sous_titre VARCHAR(255) DEFAULT NULL'); */
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
      /*   $this->addSql('ALTER TABLE forfait ADD sous_titre VARCHAR(255) DEFAULT NULL, DROP fr_sous_titre, DROP en_nom, DROP en_sous_titre, DROP en_avantages, DROP en_description, CHANGE fr_nom nom VARCHAR(255) NOT NULL, CHANGE fr_avantages avantages JSON NOT NULL, CHANGE fr_description description LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E638D7A6F6E0');
        $this->addSql('DROP INDEX IDX_4C62E638D7A6F6E0 ON contact');
        $this->addSql('ALTER TABLE equipe_dev DROP en_biographie, DROP en_image_alt, CHANGE fr_biographie biographie LONGTEXT NOT NULL, CHANGE fr_image_alt image_alt VARCHAR(255) NOT NULL'); */
    }
}
