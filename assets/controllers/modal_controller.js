import { Controller } from "@hotwired/stimulus";

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {
  connect() {
    let elements = ["vitrine", "e-commerce", "customisable"]; //tableau contenant les noms des boutons

    function modal(elements) {
      const x = document.getElementsByTagName("body")[0]; // Select body tag because of disable scroll when modal is active
      const modal = document.getElementById(elements + "-modal"); // modal
      const modalBtn = document.getElementById(elements + "-button"); // launch modal button
      const modalClose = document.getElementsByClassName("modal-close"); // close modal button

      // Open modal
      modalBtn.onclick = function () {
        modal.style.display = "flex"; // Show modal
        x.style.overflow = "hidden"; //Disable scroll on body
      };

      // Select and trigger all close buttons
      for (var i = 0; i < modalClose.length; i++) {
        modalClose[i].addEventListener("click", function () {
          modal.style.display = "none"; // Hide modal
          x.style.overflow = "auto"; // Active scroll on body
        });
      }

      // Close modal when click away from modal
      window.onclick = function (event) {
        if (event.target == modal) {
          modal.style.display = "none"; // Hide modal
          x.style.overflow = "auto"; // Active scroll on body
          console.log("click modal");
        } 
      };
    }

    for (let index = 0; index < elements.length; index++) {
      modal(elements[index]);
    }
  }
}
