-- MySQL dump 10.13  Distrib 8.0.37, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: formcodeur_db
-- ------------------------------------------------------
-- Server version	8.0.37-0ubuntu0.22.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20240614163945','2024-06-14 16:39:51',96),('DoctrineMigrations\\Version20240614183616','2024-06-14 18:36:21',73),('DoctrineMigrations\\Version20240614185112','2024-06-14 18:51:15',64),('DoctrineMigrations\\Version20240615200422','2024-06-15 20:04:30',45),('DoctrineMigrations\\Version20240616151941','2024-06-16 15:19:47',163),('DoctrineMigrations\\Version20240616161244','2024-06-16 16:12:50',39),('DoctrineMigrations\\Version20240616172503','2024-06-16 17:25:09',30);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;

--
-- Table structure for table `equipe_dev`
--

DROP TABLE IF EXISTS `equipe_dev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipe_dev` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biographie` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe_dev`
--

/*!40000 ALTER TABLE `equipe_dev` DISABLE KEYS */;
INSERT INTO `equipe_dev` VALUES (1,'Jérôme Bastaraud-Sophia','CTO - Chief Technical Officier','<div>Je m’appelle <strong>Jérôme</strong>, et je suis un <strong>développeur web</strong> animé par une passion dévorante pour l’informatique. <br>Avec mon associé, nous avons créé une agence digitale où notre art est de concevoir <strong>des sites web personnalisés</strong>. <br>Notre objectif ?<strong> Booster l’activité</strong> commerciale de nos clients. <br>Nous mettons notre expertise au service de leurs ambitions, transformant chaque idée en une<strong> solution web intuitive et efficace</strong>. <br>Chaque projet est une aventure, et en tant que pilier technique et visionnaire, je m’assure que cette aventure mène au succès dans le monde numérique. Notre travail n’est pas seulement un métier, c’est une passion qui <strong>donne vie aux entreprises</strong> dans l’univers digital.<br><br></div><div><br></div>','jerome-666e01ca51f38495587331.jpg','127558','Photo du développeur Jérôme Bastaraud-Sophia'),(2,'Michaël Serva','CIO - Chief Information Officer','<div>Je suis <strong>Michaël</strong>, un développeur web spécialisé sur la technologie Symfony, et je travaille en étroite collaboration avec mon associé, fondateur de la société <strong>Formcodeur</strong>. <br>Notre philosophie est simple : la <strong>satisfaction </strong>client avant tout. Nous nous engageons à fournir des solutions web qui non seulement répondent aux<strong> besoins spécifiques de nos clients</strong> mais les dépassent. En tant que fervent défenseur de la <strong>qualité </strong>et de <strong>l’innovation</strong>, <br>je m’assure que chaque ligne de code contribue à une expérience <strong>utilisateur exceptionnelle</strong>. <br>Notre collaboration est le <strong>moteur de notre succès</strong>, et ensemble, nous transformons les visions de nos clients en <strong>réalités digitales captivantes</strong>.<br><br></div><div><br></div>','mike-666e017e221f6455515832.jpg','101055','Photo du développeur Michaël Serva');
/*!40000 ALTER TABLE `equipe_dev` ENABLE KEYS */;

--
-- Table structure for table `forfait`
--

DROP TABLE IF EXISTS `forfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forfait` (
  `id` int NOT NULL AUTO_INCREMENT,
  `is_popular` tinyint(1) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` int DEFAULT NULL,
  `avantages` json NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `modal_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sous_titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordre` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forfait`
--

/*!40000 ALTER TABLE `forfait` DISABLE KEYS */;
INSERT INTO `forfait` VALUES (1,1,'Site vitrine',500,'[\"Une seule page\", \"Visibilité et positionnement de votre site web sur les moteurs de recherche(SEO)\", \"Charte graphique\", \"Plus d\'options sr demande\"]','<div><br></div><ol><li><strong>1 page</strong> :<br><br><ul><li>Conception d’une <strong>page unique</strong> avec un design simple et professionnel.</li><li>Intégration de votre logo et de vos couleurs d’entreprise.</li><li>Contenu basique, incluant une brève description de votre activité et vos coordonnées.</li><li>Formulaire de contact pour que les visiteurs puissent vous joindre.</li></ul></li><li><strong>Optimisation SEO de base</strong> :<br><br><ul><li>Utilisation de mots-clés pertinents dans le contenu.</li><li>Configuration des balises méta (titre, description).</li><li>Soumission du site aux moteurs de recherche.</li></ul></li><li><strong>Hébergement et nom de domaine</strong> :<br><br><ul><li>Vous devrez peut-être acheter un nom de domaine (environ 10 € par an) et souscrire à un hébergement (environ 50 € par an).</li></ul></li><li><strong>Maintenance</strong> :<br><br><ul><li>La maintenance du site n’est pas incluse dans le prix initial. Vous devrez peut-être payer des frais supplémentaires pour les mises à jour, la sécurité et le support technique.</li></ul></li></ol><div>Gardez à l’esprit que ce tarif est une estimation générale et peut varier en fonction du prestataire et des fonctionnalités spécifiques que vous souhaitez inclure. N’hésitez pas à demander des devis détaillés à des professionnels du web pour obtenir des informations plus précises. 😊🌐<br><br></div><div><br></div>','vitrine','Votre vitrine en ligne : un site web qui parle de vous !',2),(2,0,'E-Commerce',800,'[\"Cinq utilisateurs\", \"Design et personalisation\", \"Système de paiement sécurisé\", \"Création d\'un Back-office\"]','<div><br></div><ol><li><strong>Choix de la plateforme e-commerce</strong> :<br><br><ul><li>Le prix dépendra de la solution que vous choisissez. Voici quelques options populaires :&nbsp;<ul><li><strong>Magento</strong> : Solution puissante, mais nécessite des compétences techniques.</li><li><strong>WooCommerce</strong> : Plugin pour WordPress, idéal pour les petites boutiques.</li><li><strong>Prestashop</strong> : Plateforme open-source avec une communauté active.</li><li><strong>Shopify</strong> : Solution tout-en-un, conviviale pour les débutants.</li><li><strong>Drupal Commerce</strong> : Intégration avec le CMS Drupal.</li></ul></li><li>Chaque plateforme a ses avantages et inconvénients.</li></ul></li><li><strong>Design et personnalisation</strong> :<br><br><ul><li>Un design sur mesure coûte plus cher qu’un modèle préconçu.</li><li>Incluez des fonctionnalités spécifiques (panier, paiement, etc.).</li></ul></li><li><strong>Nombre de produits</strong> :<br><br><ul><li>Plus vous avez de produits, plus le coût augmentera.</li><li>Les frais de développement et d’intégration dépendent du volume.</li></ul></li><li><strong>Fonctionnalités supplémentaires</strong> :<br><br><ul><li>Intégration de modules (SEO, paiement, livraison, etc.).</li><li>Multilingue, blog, newsletter, etc.</li></ul></li><li><strong>Maintenance et hébergement</strong> :<br><br><ul><li>Prévoyez des coûts pour l’hébergement, la sécurité et les mises à jour.</li></ul></li><li><strong>Stratégie SEO</strong> :<br><br><ul><li>L’optimisation pour les moteurs de recherche peut influencer le prix.</li></ul></li><li><strong>Tarifs des prestataires</strong> :<br><br><ul><li>Les freelances et agences facturent différemment. Obtenez des devis personnalisés.</li></ul></li></ol><div>Gardez à l’esprit que ces estimations sont générales et peuvent varier en fonction de votre projet spécifique et du prestataire choisi. N’hésitez pas à demander des devis détaillés pour obtenir des tarifs précis. 😊🌐<br><br></div><div><br></div><div><br></div>','e-commerce','Boostez votre activité grâce à nos solutions sur mesure !',1),(3,0,'Customisable',NULL,'[\"Dix utilisateurs\", \"Aide à la création\"]','<div>lorem</div>','customisable','Advanced features for scalling your bussiness',3);
/*!40000 ALTER TABLE `forfait` ENABLE KEYS */;

--
-- Table structure for table `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messenger_messages` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `available_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `delivered_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messenger_messages`
--

/*!40000 ALTER TABLE `messenger_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messenger_messages` ENABLE KEYS */;

--
-- Table structure for table `site_model`
--

DROP TABLE IF EXISTS `site_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_model` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_size` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_model`
--

/*!40000 ALTER TABLE `site_model` DISABLE KEYS */;
INSERT INTO `site_model` VALUES (15,'Medical Fight','logomedicalfightblack-and-goldbgf-666d92e2333ab557531021.png','Logo du site Medical Fight','https://medical-fight.com/',107677,'2024-06-15 13:10:58','2024-06-15 08:56:05'),(16,'Karaisone','karaisone-removebg-preview-666d825d95cea511604084.png','Logo du site Karaisone','https://karaisone.fr/',21928,'2024-06-15 12:00:29','2024-06-15 09:56:56'),(17,'Maisonphilo','logo-1-666d8683f3ad8760310784.png','Logo du site Maison Philo','https://afrique.maisonphilo.com/',5971,'2024-06-15 12:18:11','2024-06-15 12:18:11'),(18,'Marc Olivier','logo-a47a3fa40821fcfc28830476aacb5230-666d896b8ae74805745575.png','Logo du site de Marc Olivier - Magnétiseur','https://site.marc-olivier-magnetiseur.com/',7570,'2024-06-15 12:30:35','2024-06-15 12:30:19'),(19,'Trans\'Air Beauvais','logo-666d8dc4b9aba127343055.png','Logo du site Trans\'Air Beauvais','https://www.trans-air-beauvais.com/',167287,'2024-06-15 12:49:08','2024-06-15 12:47:01');
/*!40000 ALTER TABLE `site_model` ENABLE KEYS */;

--
-- Dumping routines for database 'formcodeur_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-06-16 21:18:29
