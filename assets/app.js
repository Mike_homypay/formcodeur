
import "./bootstrap.js";
/*
 * Welcome to your app's main JavaScript file!
 *
 * This file will be included onto the page via the importmap() Twig function,
 * which should already be in your base.html.twig.
 */
import "./styles/app.css";

console.log("This log comes from assets/app.js - welcome to AssetMapper! 🎉");

import "./vendor/preline/preline.index.js";

  window.axeptioSettings = {
    clientId: "6694c03e502fe94cf4428752",
    cookiesVersion: "localhost:8000-fr-EU",
    googleConsentMode: {
      default: {
        analytics_storage: "denied",
        ad_storage: "denied",
        ad_user_data: "denied",
        ad_personalization: "denied",
        wait_for_update: 500,
      },
    },
  };

  (function (d, s) {
    var t = d.getElementsByTagName(s)[0],
      e = d.createElement(s);
    e.async = true;
    e.src = "//static.axept.io/sdk.js";
    t.parentNode.insertBefore(e, t);
  })(document, "script");