-- MySQL dump 10.13  Distrib 8.0.37, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: formcodeur_db
-- ------------------------------------------------------
-- Server version	8.0.37-0ubuntu0.22.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pseudo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_de_site_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4C62E638D7A6F6E0` (`type_de_site_id`),
  CONSTRAINT `FK_4C62E638D7A6F6E0` FOREIGN KEY (`type_de_site_id`) REFERENCES `type_de_site` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (12,'servam95@gmail.com','serva','06440058727','jk jkj',1),(13,'servam95@gmail.com','serva','0656878','{{ field_label(input_name) }}',3),(14,'servam95@gmail.com','serva',NULL,'{{ field_label(input_name) }}',2),(15,'servam95@gmail.com','serva','0644005872','# config/packages/webpack_encore.yaml\nwebpack_encore:\n    # ...\n\n    script_attributes:\n        defer: true\n        \'data-turbo-track\': reload\n    link_attributes:\n        \'data-turbo-track\': reload',2),(16,'ser@gmail.com','serva','0644005872','70216030sq bk; j dqq',1),(17,'servam95@gmail.com','serva','0644005872','6LcUMhAqAAAAAGha-L5MAz1IUMVJ4g3lnU1ReOJW',2);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20240614163945','2024-06-14 16:39:51',96),('DoctrineMigrations\\Version20240614183616','2024-06-14 18:36:21',73),('DoctrineMigrations\\Version20240614185112','2024-06-14 18:51:15',64),('DoctrineMigrations\\Version20240615200422','2024-06-15 20:04:30',45),('DoctrineMigrations\\Version20240616151941','2024-06-16 15:19:47',163),('DoctrineMigrations\\Version20240616161244','2024-06-16 16:12:50',39),('DoctrineMigrations\\Version20240616172503','2024-06-16 17:25:09',30),('DoctrineMigrations\\Version20240628182535','2024-06-28 18:25:40',41),('DoctrineMigrations\\Version20240629175229','2024-06-29 17:52:40',39),('DoctrineMigrations\\Version20240701184223','2024-07-01 18:42:36',38),('DoctrineMigrations\\Version20240701184536','2024-07-01 18:45:39',138),('DoctrineMigrations\\Version20240714091035','2024-07-14 09:14:33',37),('DoctrineMigrations\\Version20240714091740','2024-07-14 09:20:25',45),('DoctrineMigrations\\Version20240714093118','2024-07-14 09:31:21',31),('DoctrineMigrations\\Version20240714093832','2024-07-14 09:38:36',36),('DoctrineMigrations\\Version20240714095744','2024-07-14 09:57:47',66),('DoctrineMigrations\\Version20240714100006','2024-07-14 10:00:09',51),('DoctrineMigrations\\Version20240714103639','2024-07-14 10:36:42',63),('DoctrineMigrations\\Version20240714104040','2024-07-14 10:40:44',56),('DoctrineMigrations\\Version20240714104223','2024-07-14 10:42:25',57),('DoctrineMigrations\\Version20240714104340','2024-07-14 10:43:47',50),('DoctrineMigrations\\Version20240714104519','2024-07-14 10:45:22',46),('DoctrineMigrations\\Version20240716052124','2024-07-16 05:21:29',37);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;

--
-- Table structure for table `equipe_dev`
--

DROP TABLE IF EXISTS `equipe_dev`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipe_dev` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `titre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fr_biographie` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_size` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fr_image_alt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_biographie` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `en_image_alt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe_dev`
--

/*!40000 ALTER TABLE `equipe_dev` DISABLE KEYS */;
INSERT INTO `equipe_dev` VALUES (1,'Jérôme Bastaraud-Sophia','CTO - Chief Technical Officier','<div>Je m’appelle <strong>Jérôme</strong>, et je suis un <strong>développeur web</strong> animé par une passion dévorante pour l’informatique. <br>Avec mon associé, nous avons créé une agence digitale où notre art est de concevoir <strong>des sites web personnalisés</strong>. <br>Notre objectif ?<strong> Booster l’activité</strong> commerciale de nos clients. <br>Nous mettons notre expertise au service de leurs ambitions, transformant chaque idée en une<strong> solution web intuitive et efficace</strong>. <br>Chaque projet est une aventure, et en tant que pilier technique et visionnaire, je m’assure que cette aventure mène au succès dans le monde numérique. Notre travail n’est pas seulement un métier, c’est une passion qui <strong>donne vie aux entreprises</strong> dans l’univers digital.</div>','jerome-666e01ca51f38495587331.jpg','127558','Photo du développeur Jérôme Bastaraud-Sophia','<div>My name is <strong>Jérôme</strong>, and I\'m a <strong>web developer </strong>with an all-consuming passion for computers.<br>With my partner, we\'ve set up a digital agency where our art is designing <strong>custom websites.</strong><br>Our aim? To <strong>boost our customers\' commercial activity</strong>.<br>We put our expertise at the service of their ambitions, transforming every idea into an intuitive and <strong>effective web solution.</strong><br>Each project is an adventure, and as the technical backbone and visionary, I ensure that this adventure leads to success in the digital world. Our work is not just a job, it\'s a passion that <strong>brings businesses to life in the digital world.</strong></div>','Photo of developer Jérôme Bastaraud-Sophia'),(2,'Michaël Serva','CIO - Chief Information Officer','<div>Je suis <strong>Michaël</strong>, un développeur web spécialisé sur la technologie Symfony, et je travaille en étroite collaboration avec mon associé, fondateur de la société <strong>Formcodeur</strong>. <br>Notre philosophie est simple : la <strong>satisfaction </strong>client avant tout. Nous nous engageons à fournir des solutions web qui non seulement répondent aux<strong> besoins spécifiques de nos clients</strong> mais les dépassent. En tant que fervent défenseur de la <strong>qualité </strong>et de <strong>l’innovation</strong>, <br>je m’assure que chaque ligne de code contribue à une expérience <strong>utilisateur exceptionnelle</strong>. <br>Notre collaboration est le <strong>moteur de notre succès</strong>, et ensemble, nous transformons les visions de nos clients en <strong>réalités digitales captivantes</strong>.<br><br></div><div><br></div>','mike-666e017e221f6455515832.jpg','101055','Photo du développeur Michaël Serva','<div>I\'m <strong>Michaël</strong>, a web developer specialising in <strong>Symfony technology</strong>, and I work closely with my partner, the founder of <strong>Formcodeur</strong>.<br>Our philosophy is simple: <strong>customer satisfaction</strong> comes first. We\'re committed to delivering web solutions that not only meet but <strong>exceed our customers\' specific needs</strong>. As a firm believer in quality and innovation,<br>I ensure that every line of code contributes to an <strong>exceptional user experience</strong>.<br>Collaboration is the <strong>driving force behind our success</strong>, and together we transform our customers\' visions into <strong>exciting digital realities</strong>.</div>',NULL);
/*!40000 ALTER TABLE `equipe_dev` ENABLE KEYS */;

--
-- Table structure for table `forfait`
--

DROP TABLE IF EXISTS `forfait`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forfait` (
  `id` int NOT NULL AUTO_INCREMENT,
  `is_popular` tinyint(1) DEFAULT NULL,
  `fr_nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` int DEFAULT NULL,
  `fr_avantages` json NOT NULL,
  `fr_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `modal_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fr_sous_titre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordre` smallint DEFAULT NULL,
  `en_nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_sous_titre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en_avantages` json DEFAULT NULL,
  `en_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forfait`
--

/*!40000 ALTER TABLE `forfait` DISABLE KEYS */;
INSERT INTO `forfait` VALUES (1,1,'Site vitrine',500,'[\"Une seule page\", \"Visibilité et positionnement de votre site web sur les moteurs de recherche(SEO)\", \"Charte graphique\", \"Plus d\'options sur demande\"]','<div><br></div><ol><li><strong>1 page</strong> :<br><br><ul><li>Conception d’une <strong>page unique</strong> avec un design simple et professionnel.</li><li>Intégration de votre logo et de vos couleurs d’entreprise.</li><li>Contenu basique, incluant une brève description de votre activité et vos coordonnées.</li><li>Formulaire de contact pour que les visiteurs puissent vous joindre.</li></ul></li><li><strong>Optimisation SEO de base</strong> :<br><br><ul><li>Utilisation de mots-clés pertinents dans le contenu.</li><li>Configuration des balises méta (titre, description).</li><li>Soumission du site aux moteurs de recherche.</li></ul></li><li><strong>Hébergement et nom de domaine</strong> :<br><br><ul><li>Vous devrez peut-être acheter un nom de domaine (environ 10 € par an) et souscrire à un hébergement (environ 50 € par an).</li></ul></li><li><strong>Maintenance</strong> :<br><br><ul><li>La maintenance du site n’est pas incluse dans le prix initial. Vous devrez peut-être payer des frais supplémentaires pour les mises à jour, la sécurité et le support technique.</li></ul></li></ol><div>Gardez à l’esprit que ce tarif est une estimation générale et peut varier en fonction du prestataire et des fonctionnalités spécifiques que vous souhaitez inclure. N’hésitez pas à demander des devis détaillés à des professionnels du web pour obtenir des informations plus précises. 😊🌐<br><br></div><div><br></div>','vitrine','Votre vitrine en ligne : un site web qui parle de vous !',2,'Showcase website','Your online showcase: a website that\'s all about you!','[\"A single page\", \"Visibility and positioning of your website on search engines (SEO)\", \"Graphic charter\", \"More options on request\"]','<div>1 page :<br><br>Creation of a single page with a simple, professional design.<br>Incorporation of your logo and corporate colours.<br>Basic content, including a brief description of your business and your contact details.<br>Contact form so visitors can reach you.<br>Basic SEO optimisation:<br><br>Use of relevant keywords in the content.<br>Configuration of meta tags (title, description).<br>Submission of the site to search engines.<br>Hosting and domain name:<br><br>You may need to purchase a domain name (around €10 per year) and sign up for hosting (around €50 per year).<br>Maintenance :<br><br>Site maintenance is not included in the initial price. You may have to pay extra for updates, security and technical support.<br>Bear in mind that this price is a general estimate and may vary depending on the service provider and the specific features you wish to include. Don\'t hesitate to request detailed quotes from web professionals for more accurate information. 😊🌐<br><br></div>'),(2,0,'E-Commerce - Vente en ligne',800,'[\"Cinq utilisateurs\", \"Design et personalisation\", \"Système de paiement sécurisé\", \"Création d\'un Back-office\"]','<div><br></div><ol><li><strong>Design et personnalisation</strong> :<br><br><ul><li>Un design sur mesure coûte plus cher qu’un modèle préconçu.</li><li>Incluez des fonctionnalités spécifiques (panier, paiement, etc.).</li></ul></li><li><strong>Nombre de produits</strong> :<br><br><ul><li>Plus vous avez de produits, plus le coût augmentera.</li><li>Les frais de développement et d’intégration dépendent du volume.</li></ul></li><li><strong>Fonctionnalités supplémentaires</strong> :<br><br><ul><li>Intégration de modules (SEO, paiement, livraison, etc.).</li><li>Multilingue, blog, newsletter, etc.</li></ul></li><li><strong>Maintenance et hébergement</strong> :<br><br><ul><li>Prévoyez des coûts pour l’hébergement, la sécurité et les mises à jour.</li></ul></li><li><strong>Stratégie SEO</strong> :<br><br><ul><li>L’optimisation pour les moteurs de recherche peut influencer le prix.</li></ul></li><li><strong>Tarifs des prestataires</strong> :<br><br><ul><li>Les freelances et agences facturent différemment. Obtenez des devis personnalisés.</li></ul></li></ol><div>Gardez à l’esprit que ces estimations sont générales et peuvent varier en fonction de votre projet spécifique et du prestataire choisi. N’hésitez pas à demander des devis détaillés pour obtenir des tarifs précis. 😊🌐<br><br></div><div><br></div><div><br></div>','e-commerce','Boostez votre activité grâce à nos solutions sur mesure !',1,'E-Commerce','Boost your business with our tailor-made solutions!','[\"Five users\", \"Design and personalisation\", \"Secure payment system\", \"Creation of a back office\"]','<div><br>Design and customisation:<br><br>A customised design costs more than a preconceived template.<br>Include specific functionalities (shopping basket, payment, etc.).<br>Number of products:<br><br>The more products you have, the higher the cost.<br>Development and integration costs depend on volume.<br>Additional features :<br><br>Integration of modules (SEO, payment, delivery, etc.).<br>Multilingual, blog, newsletter, etc.<br>Maintenance and hosting:<br><br>Expect costs for hosting, security and updates.<br>SEO strategy:<br><br>Search engine optimisation can influence the price.<br>Service provider rates:<br><br>Freelancers and agencies charge differently. Get personalised quotes.<br>Bear in mind that these estimates are general and may vary depending on your specific project and the service provider you choose. Don\'t hesitate to ask for detailed quotes to get accurate rates. 😊🌐<br><br><br></div>'),(3,0,'Customisable',NULL,'[\"Tous projets\", \"Aide à la création\"]','<div><br></div><ul><li><strong>Personnalisation totale</strong> : Chaque site est conçu sur mesure pour refléter votre vision unique et vos besoins spécifiques.</li><li><strong>Design créatif</strong> : Nous utilisons des designs innovants et modernes pour capturer l’essence de votre marque.</li><li><strong>Fonctionnalités sur mesure</strong> : Intégration de fonctionnalités personnalisées pour répondre à vos exigences spécifiques, qu’il s’agisse de galeries, de blogs, de boutiques en ligne, etc.</li><li><strong>Expérience utilisateur optimisée</strong> : Création d’une navigation intuitive et fluide pour offrir une expérience utilisateur exceptionnelle.</li><li><strong>Adaptabilité mobile</strong> : Tous nos sites sont entièrement responsives, garantissant une expérience optimale sur tous les appareils.</li><li><strong>Support continu</strong> : Nous offrons un support technique et des mises à jour régulières pour assurer la pérennité et la performance de votre site.</li><li><strong>Liberté créative</strong> : Vous avez la possibilité de participer activement au processus de création, en apportant vos idées et suggestions à chaque étape.</li></ul><div><br></div><div><br></div><div><br></div>','customisable','Fonctionnalités avancées pour l\'établissement de votre entreprise',3,'Customisable','Advanced features for scalling your bussiness','[\"All projects\", \"Creation aid\"]','<div>Of course you can! Here\'s a bulleted text to suggest the creation of a website that gives free rein to the client\'s imagination:<br><br>- **Total customisation**: Each site is tailor-made to reflect your unique vision and specific needs.<br>- Creative design**: We use innovative and modern designs to capture the essence of your brand.<br>- Tailor-made features**: Integration of custom features to meet your specific requirements, whether for galleries, blogs, online shops, etc.<br>- Optimised user experience**: Creation of intuitive, fluid navigation for an exceptional user experience.<br>- Mobile adaptability**: All our sites are fully responsive, guaranteeing an optimal experience on all devices.<br>- Ongoing support**: We offer technical support and regular updates to ensure your site\'s longevity and performance.<br>- Creative freedom**: You can actively participate in the design process, contributing your ideas and suggestions at every stage.<br><br>Does it match what you had in mind? 😊<br><br>Source: conversation with Copilot, 14/07/2024<br>(1) How to create a website in 2024: Complete Guide - Website Planet. https://www.websiteplanet.com/fr/blog/comment-creer-un-site-web-guide-complet/.<br>(2) Getting clients for web design - Kinsta. https://kinsta.com/fr/blog/comment-obten</div>');
/*!40000 ALTER TABLE `forfait` ENABLE KEYS */;

--
-- Table structure for table `messenger_messages`
--

DROP TABLE IF EXISTS `messenger_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messenger_messages` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `headers` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue_name` varchar(190) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `available_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `delivered_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`),
  KEY `IDX_75EA56E0FB7336F0` (`queue_name`),
  KEY `IDX_75EA56E0E3BD61CE` (`available_at`),
  KEY `IDX_75EA56E016BA31DB` (`delivered_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messenger_messages`
--

/*!40000 ALTER TABLE `messenger_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messenger_messages` ENABLE KEYS */;

--
-- Table structure for table `site_model`
--

DROP TABLE IF EXISTS `site_model`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `site_model` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_alt` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_size` int DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `en_image_alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_model`
--

/*!40000 ALTER TABLE `site_model` DISABLE KEYS */;
INSERT INTO `site_model` VALUES (15,'Medical Fight','logomedicalfightblack-and-goldbgf-666d92e2333ab557531021.png','Logo du site Medical Fight','https://medical-fight.com/',107677,'2024-06-15 13:10:58','2024-06-15 08:56:05','Medical Fight website logo'),(16,'Karaisone','karaisone-removebg-preview-666d825d95cea511604084.png','Logo du site Karaisone','https://karaisone.fr/',21928,'2024-06-15 12:00:29','2024-06-15 09:56:56','Karaison website logo'),(17,'Maisonphilo','logo-1-666d8683f3ad8760310784.png','Logo du site Maison Philo','https://afrique.maisonphilo.com/',5971,'2024-06-15 12:18:11','2024-06-15 12:18:11','Maison Philo website logo'),(18,'Marc Olivier','logo-a47a3fa40821fcfc28830476aacb5230-666d896b8ae74805745575.png','Logo du site de Marc Olivier - Magnétiseur','https://site.marc-olivier-magnetiseur.com/',7570,'2024-06-15 12:30:35','2024-06-15 12:30:19','Marc Olivier - Magnetizer website logo'),(19,'Trans\'Air Beauvais','logo-666d8dc4b9aba127343055.png','Logo du site Trans\'Air Beauvais','https://www.trans-air-beauvais.com/',167287,'2024-06-15 12:49:08','2024-06-15 12:47:01','Trans\'Air Beauvais website logo'),(20,'Ermont Football','logo-669602fe6c384052672048.jpeg','Logo du site web du club d\'Ermont','https://ermont.maisonphilo.com/',36191,'2024-07-16 05:19:58','2024-07-16 05:19:58','Ermont club website logo');
/*!40000 ALTER TABLE `site_model` ENABLE KEYS */;

--
-- Table structure for table `type_de_site`
--

DROP TABLE IF EXISTS `type_de_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_de_site` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordre` smallint DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_de_site`
--

/*!40000 ALTER TABLE `type_de_site` DISABLE KEYS */;
INSERT INTO `type_de_site` VALUES (1,'Site Vitrine',1),(2,'Autre',5),(3,'E-commerce',2),(4,'Blog',4);
/*!40000 ALTER TABLE `type_de_site` ENABLE KEYS */;

--
-- Dumping routines for database 'formcodeur_db'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-07-16 11:04:51
