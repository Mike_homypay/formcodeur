<?php

namespace App\Entity;

use App\Repository\EquipeDevRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EquipeDevRepository::class)]
#[Vich\Uploadable]
class EquipeDev
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $titre = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $frBiographie = null;

    // NOTE: This is not a mapped field of entity metadata, just a simple property.
    #[Vich\UploadableField(mapping: 'equipe_dev', fileNameProperty: 'imageName', size: 'imageSize')]
    private ?File $imageFile = null;

    #[ORM\Column(length: 255)]
    private ?string $imageName = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $imageSize = null;

    #[ORM\Column(length: 255)]
    private ?string $frImageAlt = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $enBiographie = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $enImageAlt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): static
    {
        $this->titre = $titre;

        return $this;
    }

    public function getFrBiographie(): ?string
    {
        return $this->frBiographie;
    }

    public function setFrBiographie(string $frBiographie): static
    {
        $this->frBiographie = $frBiographie;

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;
        /*
                if (null !== $imageFile) {
                    // It is required that at least one field changes if you are using doctrine
                    // otherwise the event listeners won't be called and the file is lost
                    $this->updatedAt = new \DateTimeImmutable();
                } */
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageName(?string $imageName): static
    {
        $this->imageName = $imageName;

        return $this;
    }

    public function getImageSize(): ?string
    {
        return $this->imageSize;
    }

    public function setImageSize(?string $imageSize): static
    {
        $this->imageSize = $imageSize;

        return $this;
    }

    public function getFrImageAlt(): ?string
    {
        return $this->frImageAlt;
    }

    public function setFrImageAlt(string $frImageAlt): static
    {
        $this->frImageAlt = $frImageAlt;

        return $this;
    }

    public function getEnBiographie(): ?string
    {
        return $this->enBiographie;
    }

    public function setEnBiographie(?string $enBiographie): static
    {
        $this->enBiographie = $enBiographie;

        return $this;
    }

    public function getEnImageAlt(): ?string
    {
        return $this->enImageAlt;
    }

    public function setEnImageAlt(?string $enImageAlt): static
    {
        $this->enImageAlt = $enImageAlt;

        return $this;
    }
}
