<?php

namespace App\Entity;

use App\Repository\ForfaitRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ForfaitRepository::class)]
class Forfait
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isPopular = null;

    #[ORM\Column(length: 255)]
    private ?string $frNom = null;

    #[ORM\Column(nullable: true)]
    private ?int $prix = null;

    #[ORM\Column]
    private array $frAvantages = [];

    #[ORM\Column(type: Types::TEXT)]
    private ?string $frDescription = null;

    #[ORM\Column(length: 255)]
    private ?string $modalId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $frSousTitre = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: true)]
    #[ORM\OrderBy(['ordre' => 'ASC'])]
    private ?int $ordre = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $enNom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $enSousTitre = null;

    #[ORM\Column(nullable: true)]
    private ?array $enAvantages = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $enDescription = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isPopular(): ?bool
    {
        return $this->isPopular;
    }

    public function setIsPopular(?bool $isPopular): static
    {
        $this->isPopular = $isPopular;

        return $this;
    }

    public function getFrNom(): ?string
    {
        return $this->frNom;
    }

    public function setFrNom(string $frNom): static
    {
        $this->frNom = $frNom;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    public function getFrAvantages(): array
    {
        return $this->frAvantages;
    }

    public function setFrAvantages(array $frAvantages): static
    {
        $this->frAvantages = $frAvantages;

        return $this;
    }

    public function getFrDescription(): ?string
    {
        return $this->frDescription;
    }

    public function setFrDescription(string $frDescription): static
    {
        $this->frDescription = $frDescription;

        return $this;
    }

    public function getModalId(): ?string
    {
        return $this->modalId;
    }

    public function setModalId(string $modalId): static
    {
        $this->modalId = $modalId;

        return $this;
    }

    public function getFrSousTitre(): ?string
    {
        return $this->frSousTitre;
    }

    public function setFrSousTitre(?string $frSousTitre): static
    {
        $this->frSousTitre = $frSousTitre;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): static
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getEnNom(): ?string
    {
        return $this->enNom;
    }

    public function setEnNom(?string $enNom): static
    {
        $this->enNom = $enNom;

        return $this;
    }

    public function getEnSousTitre(): ?string
    {
        return $this->enSousTitre;
    }

    public function setEnSousTitre(?string $enSousTitre): static
    {
        $this->enSousTitre = $enSousTitre;

        return $this;
    }

    public function getEnAvantages(): ?array
    {
        return $this->enAvantages;
    }

    public function setEnAvantages(?array $enAvantages): static
    {
        $this->enAvantages = $enAvantages;

        return $this;
    }

    public function getEnDescription(): ?string
    {
        return $this->enDescription;
    }

    public function setEnDescription(?string $enDescription): static
    {
        $this->enDescription = $enDescription;

        return $this;
    }
}
