<?php

namespace App\Form;

use App\Entity\Contact;
use App\Entity\TypeDeSite;
use Symfony\Component\Form\AbstractType;
use Symfonycasts\DynamicForms\DependentField;
use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfonycasts\DynamicForms\DynamicFormBuilder;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder = new DynamicFormBuilder($builder);

        $builder
            ->add('pseudo', TextType::class, [
                'required' => true,
                'attr' => [
                    'placeholder' => 'Votre nom ou celui de votre société'
                ],
                'constraints' => [
                    new NotBlank(
                        [
                            'message' => 'Veuillez renseigner votre nom ou votre société'
                        ]
                    ),
                    new Length(
                        [
                            'max' => 50,
                            'maxMessage' => 'Votre nom ou votre société doit contenir au maximum {{ limit }} caractères',
                        ]
                    )
                ]
            ])
            ->addDependent(
                'email',
                'pseudo',
                function (DependentField $field, ?string $pseudo) {
                    if ($pseudo) {
                        $field->add(TextType::class, [
                            'required' => true,

                            'attr' => [
                                'placeholder' => 'Votre email'
                            ],
                            'constraints' => [
                                new NotBlank(
                                    [
                                        'message' => 'Veuillez renseigner votre email'
                                    ]
                                ),
                                new Length(
                                    [
                                        'max' => 250,
                                        'maxMessage' => 'Votre email doit contenir au maximum {{ limit }} caractères',
                                    ]
                                ),
                            ]
                        ]);
                    }
                }
            )


            ->addDependent('type_de_site', 'email', function (DependentField $field, ?string $email) {
                if ($email) {
                    $field->add(EntityType::class, [
                        'class' => TypeDeSite::class,
                        //  'required' => true,
                        //   'choice_label' => 'Quel type de site souhaitez-vous créer?',
                    ]);
                }
            })

            ->addDependent('telephone', 'message', function (DependentField $field, ?string $message) {
                if ($message) {
                    $field
                        ->add(TextType::class, [
                            'required' => true,
                            'attr' => [
                                'placeholder' => 'Votre numéro de téléphone',
                                'class' => 'rounded-lg'
                            ],
                            'constraints' => [

                                new Length(
                                    [
                                        'max' => 20,
                                        'maxMessage' => 'Votre numéro de téléphone doit contenir au maximum {{ limit }} caractères',
                                    ]
                                )

                            ]
                        ]);
                }
            })

            ->addDependent('message', 'type_de_site', function (DependentField $field, ?string $objet) {
                if ($objet) {
                    $field
                        ->add(TextareaType::class, [
                            'required' => true,
                            'label' => 'Message',
                            'attr' => [
                                'placeholder' => 'Votre message'
                            ],
                            'constraints' => [
                                new NotBlank(
                                    [
                                        'message' => 'Veuillez renseigner votre message'
                                    ]
                                ),
                                new Length(
                                    [
                                        'max' => 500,
                                        'maxMessage' => 'Votre message doit contenir au maximum {{ limit }} caractères',
                                    ]
                                )
                            ]
                        ]);
                }
            })


            ->add('submit', SubmitType::class, [
                'label' => 'Envoyer',
            ])

            ->add('captcha', Recaptcha3Type::class, [
                'constraints' => new Recaptcha3(),
                'action_name' => 'homepage',
                //'script_nonce_csp' => $nonceCSP,
                'locale' => 'fr',
                'constraints' => new Recaptcha3([
                    'message' => 'karser_recaptcha3.message',
                    'messageMissingValue' => 'karser_recaptcha3.message_missing_value',
                ]),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
