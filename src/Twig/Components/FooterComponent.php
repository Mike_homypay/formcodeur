<?php

namespace App\Twig\Components;

use App\Repository\EquipeDevRepository;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
final class FooterComponent
{
    use DefaultActionTrait;

    private EquipeDevRepository $equipes;

    public function __construct(EquipeDevRepository $equipes)
    {
        $this->equipes = $equipes;
    }

    public function getEquipes(): array
    {
        // an example method that returns an array of Products
        return $this->equipes->findAll();
    }



}
