<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:make-user',
    description: 'Add a short description for your command',
)]
class MakeUserCommand extends Command
{
    public function __construct(
        private     RequestStack $requestStack,
        private   EntityManagerInterface $entityManager,
        private UserPasswordHasherInterface $hasher
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Veuillez inscrire votre email')
            ->addArgument('arg2', InputArgument::OPTIONAL, 'Veuillez inscrire votre mot de passe')

            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $arg2 = $input->getArgument('arg2');


        $user = new \App\Entity\User();
        $user->setEmail($arg1)->setPassword($this->hasher->hashPassword($user, $arg2))->setRoles(["ROLE_ADMIN"]);
        ;

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $io->success($user->getEmail() . 'created');

        return Command::SUCCESS;
    }
}
