<?php

namespace App\Controller\Admin;

use IntlChar;
use App\Entity\Forfait;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ForfaitCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Forfait::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            BooleanField::new('isPopular'),
            TextField::new('frNom'),
            TextField::new('enNom'),
            TextField::new('frSousTitre'),
            TextField::new('enSousTitre'),
            IntegerField::new('prix'),
            ArrayField::new('frAvantages'),
            ArrayField::new('enAvantages'),
            TextEditorField::new('frDescription'),
            TextEditorField::new('enDescription'),
            TextField::new('modalId'),
            NumberField::new('ordre'),
        ];
    }

}
