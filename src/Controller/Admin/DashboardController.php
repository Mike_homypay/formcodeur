<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use App\Entity\Forfait;
use App\Entity\EquipeDev;
use App\Entity\SiteModel;
use App\Entity\TypeDeSite;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(SiteModelCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Formcodeur');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Acceuil', 'fa fa-home', 'app_home');
        yield MenuItem::linkToCrud('Site Modèle', 'fas fa-list', SiteModel::class);
        yield MenuItem::linkToCrud('Equipe de dev', 'fas fa-user-tie', EquipeDev::class);
        yield MenuItem::linkToCrud('Forfait', 'fas fa-euro-sign', Forfait::class);
        yield MenuItem::linkToCrud('Contact', 'fas fa-sms', Contact::class);
        yield MenuItem::linkToCrud('Type_de_site', 'fas fa-shop', TypeDeSite::class);

    }
}
