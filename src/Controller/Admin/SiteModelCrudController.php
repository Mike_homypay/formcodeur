<?php

namespace App\Controller\Admin;

use App\Entity\SiteModel;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\File;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class SiteModelCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SiteModel::class;
    }


    public function createEntity(string $entityFqcn)
    {
        $siteModel = new SiteModel();
        $siteModel->setCreatedAt(new \DateTimeImmutable('now'));

        return $siteModel;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('createdAt', 'Date de l\'ajout')->onlyOnDetail(),
            TextField::new('nom'),
            ImageField::new('imageName')->setEmptyData('')->setBasePath('../assets/public/images/site_models')->onlyOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->setFormTypeOption('constraints', [
                new File([
                    'maxSize' => '2M',
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/webpp'
                    ],
                    'mimeTypesMessage' => 'Please upload a valid image. '
                ])
            ])->hideOnIndex(),
            TextField::new('imageAlt')->hideOnIndex(),
            TextField::new('enImageAlt'),
            TextField::new('path')->hideOnIndex(),

        ];
    }
}
