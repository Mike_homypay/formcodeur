<?php

namespace App\Controller\Admin;

use App\Entity\EquipeDev;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Validator\Constraints\File;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class EquipeDevCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return EquipeDev::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('nom'),
            TextField::new('titre'),
            ImageField::new('imageName')->setEmptyData('')->setBasePath('../assets/public/images/equipe_dev')->onlyOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->setFormTypeOption('constraints', [
                new File([
                    'maxSize' => '2M',
                    'mimeTypes' => [
                        'image/jpeg',
                        'image/png',
                        'image/webpp'
                    ],
                    'mimeTypesMessage' => 'Please upload a valid image. '
                ])
            ])->hideOnIndex(),
            TextField::new('frImageAlt')->hideOnIndex(),
            TextField::new('enImageAlt')->hideOnIndex(),
            TextEditorField::new('frBiographie')->hideOnIndex(),
            TextEditorField::new('enBiographie')->hideOnIndex(),


        ];
    }
}
