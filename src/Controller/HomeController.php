<?php

namespace App\Controller;

use App\Repository\ContactRepository;
use App\Repository\ForfaitRepository;
use App\Repository\EquipeDevRepository;
use App\Repository\SiteModelRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Validator\Constraints\Length;

class HomeController extends AbstractController
{
    public function __construct(
        private SiteModelRepository $siteModelRepository,
        private EquipeDevRepository $equipeDevRepository,
        private ForfaitRepository $forfaitRepository,
        private TranslatorInterface $translator,  // Add this line to use the translator.
        private ContactRepository $contactRepository  // Add this line to use the contact repository.
    ) {
    }
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        $site_models = $this->siteModelRepository->findAll();
        $equipe_devs = $this->equipeDevRepository->findAll();
        $forfaits = $this->forfaitRepository->findByOrdre();
        $contacts = count($this->contactRepository->findAll());

        return $this->render('home/index.html.twig', [
            'site_models' => $site_models,
            'equipe_devs' => $equipe_devs,
            'forfaits' => $forfaits,
            'nb_contacts' => $contacts,
        ]);
    }

    #[Route('/change_locales/{locale}', name: 'app_change_locales')]
    public function changeLocales($locale, Request $request): Response
    {
        $request->getSession()->set('_locale', $locale);
        return $this->redirect($request->headers->get('referer'));
    }
}
