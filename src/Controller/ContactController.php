<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\EquipeDevRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    public function __construct(
        private EquipeDevRepository $equipeDevRepository,
    ) {
    }

    #[Route('/contact', name: 'app_contact')]
    public function index(
        Request $request,
        EntityManagerInterface $em,
        MailerInterface $mailer
    ): Response {
        /*    $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $contact = $form->getData();

            // Message flash pour l'utilisateur pour confirmer l'envoi du message
            $this->addFlash('success', $contact->getEmail() . 'Votre message a bien été envoyé');

            // Envoi du mail

            //enregistrement du contact en base de donnée
            $em->persist($contact);
            $em->flush();

            return $this->redirectToRoute('app_home');
        } */
        $equipe_devs = $this->equipeDevRepository->findAll();

        $form = $this->createForm(ContactType::class, options: ['action' => $this->generateUrl('app_home')]);

        $form->handleRequest($request);
        {
            /** @var SubmitButton $submitButton */
            $submitButton = $form->get('submit');
            if (!$submitButton->isClicked()) {
                return $this->render('contact/index.html.twig', ['form' => $form, 'equipe_devs' => $equipe_devs,]);
            }
            $contact = $form->getData();


            // Message flash pour l'utilisateur pour confirmer l'envoi du message
            $this->addFlash('success', $contact->getEmail() . 'Votre message a bien été envoyé');

            // Envoi du mail

            $email = (new TemplatedEmail())
                ->from($contact->getEmail())
                ->to('cto@formcodeur.com', 'cio@formcodeur.com')
                ->subject('vous avez reçu une nouvelle demande')
                ->htmlTemplate('emails/contactEmail.html.twig')
                ->context([
                    'demande' => $contact,
                ]);

            $mailer->send($email);
            //enregistrement du contact en base de donnée
            $em->persist($contact);
            $em->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->render('contact/index.html.twig', [
            // 'form' => $form
        ]);
    }
}
