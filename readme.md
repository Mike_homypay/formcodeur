# Formcodeur

##  Apache

``bash composer require symfony/apache-pack ``

## Phrase clé pour plus tard

Bien sûr ! Voici quelques suggestions de phrases d'accroche pour un créateur de site web :

1. "Transformez vos idées en sites web exceptionnels avec notre expertise."
2. "Concevons ensemble votre présence en ligne, du design à la mise en ligne."
3. "Des sites web sur mesure qui captivent et convertissent."
4. "Création web innovante pour votre entreprise."

N'hésitez pas à personnaliser ces phrases en fonction de votre style et de votre domaine d'expertise ! 😊

## Modal

https://codepen.io/umurkose/pen/ZExbmqL

## Coding Standard 

tools/php-cs-fixer/vendor/bin/php-cs-fixer fix src


### Traduction 

La commande à lancer pour remettre à jour les fichiers de traduction 
``bash  php bin/console translation:extract --force --prefix="new_" fr ``

En Bdd, bien pensez à creer des fields pour les differentes langues avec le prefixe fr ou en par exemple

## Bundle 

https://versoly.com/taos
